import { r as registerInstance, h } from './core-9a4b53af.js';

const AppRoot = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("ion-app", null, h("ion-router", { useHash: false }, h("ion-route", { url: "/", component: "app-home" }), h("ion-route", { url: "/profile/", component: "app-profile" }), h("ion-route", { url: "/sign/", component: "app-sign" }), h("ion-route", { url: "/courses/", component: "app-courses" }), h("ion-route", { url: "/addcourse/", component: "app-addcourse" }), h("ion-route", { url: "/coursecard", component: "app-coursecard" }), h("ion-route", { url: "/courselist", component: "app-courselist" }), h("ion-route", { url: "/courseinfo/:id", component: "app-courseinfo" })), h("ion-nav", null)));
    }
    static get style() { return ""; }
};

export { AppRoot as app_root };
