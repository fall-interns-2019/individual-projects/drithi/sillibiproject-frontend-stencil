import { r as registerInstance, h } from './core-9a4b53af.js';

const AppCoursecard = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    async componentWillLoad() {
        this.getCourseinfo();
    }
    async getCourseinfo() {
        const response = await fetch("http://localhost:3000/api/v1/courses");
        const coursesinfo = await response.json();
        this.courses = coursesinfo;
    }
    render() {
        return [
            h("ion-card", { href: `/courseinfo/${this.id}` }, h("ion-list", { lines: "full" }, h("ion-item", null, h("ion-text", null, " ", this.course_name, " ")), h("ion-item", { lines: 'full' }, h("ion-label", null, h("ion-note", null, this.course_number, " - ", this.section_number, " ", h("br", null), " ", this.instructor))), h("ion-item", null, h("ion-icon", { slot: "start", name: "document" }), h("ion-text", null, " Syllabus "), h("ion-icon", { slot: "end", name: "add-circle-outline" }, " ")), h("ion-item", null, h("ion-icon", { slot: "start", name: "paper" }, " "), " ", h("ion-text", null, " Assignments "), h("ion-icon", { slot: "end", name: "add-circle-outline" }, " "))))
        ];
    }
    static get style() { return ".icon{\n  padding-top: 2px;\n  margin-left: 2px;\n}\n\n.add{\n  margin-left: auto;\n}"; }
};

export { AppCoursecard as app_coursecard };
