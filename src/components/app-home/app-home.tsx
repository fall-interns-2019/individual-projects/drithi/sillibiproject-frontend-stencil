import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {

  render() {
    return [

      <ion-content color = "purple" class="ion-padding">
      <ion-grid>
        <ion-row align-items-center={true} style={{height:"100vh"}} justify-content-around={true}>
          <ion-col align-self-center={true}>
        <ion-item color ="purple">
        <img src={"../assets/icon/sillibi-logo.png"} style={{marginTop: "120px"}}/>
        </ion-item>

        <ion-button class="Fbutton" href="https://facebook.com" expand="full" style={{marginTop:"120px"}} color={"blue"}> LOGIN USING FACEBOOK </ion-button>
        <ion-button class="Ebutton" href="https://gmail.com" expand={"full"} style={{marginTop:"10px", marginBottom:"80px"}} color={"yellow"}> LOGIN USING EMAIL </ion-button>

        <p> <div text-center> Don't want to use facebook? </div> </p>
        <ion-button class="CreateAccount" href={"/profile"} fill={"clear"}>CREATE AN ACCOUNT</ion-button>
          </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>

    ];
  }
}
